
from flask import Flask, render_template, request,jsonify
import json
from langgraph.graph import Graph,END
from langchain_groq import ChatGroq


import os
os.environ["LANGCHAIN_TRACING_V2"] = "true"
os.environ["LANGCHAIN_PROJECT"] = "Lecture"


llm = ChatGroq(temperature=0)
app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')
    # return "hello world"
if __name__ == '__main__':
    app.run(debug=True)

@app.route('/ask', methods=['POST'])
def ask():
    data = request.get_json()
    question = data['question']
    response = call_grog(question)
    # print(response)
    return response
    # return jsonify({'response': response})

# @app.route('/load_json')
def load_json():
    json_path = os.path.join(app.root_path, 'static', 'data', 'lecturePayloadWithLatex2.json')
    
    with open(json_path, 'r') as json_file:
        data = json_file.read()
    return data



def call_grog(question):
    sys_msg = """You are a k-12 math teacher. You will be teaching a class using a script that will be provided.When the lecture is going on a student has some doubt. So he interrupts you and askes you a question based on the lecture that you are currently giving. Generate a new short script explaining the answer to the question asked by the student. The timestamp should start from 0 onwards. You are in a classroom and have a mouth to speak and a whiteboard on which you can write or draw to communicate with the student your thoughts. Think very clearly and systematically how you will answer this student's question/doubt. Generate a script of how you will explain this to him with the timestamp breakdown of what you will speak and what you will write or draw on the whiteboard.Output the whole as one markdown script."""

    # script = """{
    # 'title': 'Solving Equations',
    # 'events': [
    #     {
    #     'timestamp': '0:00 - 0:10',
    #     'avatar': {
    #         'speak': "Good morning 7th graders, today we're going to solve the equation 2x - 4 = 0 for x. Let's start by writing the equation on the board."
    #     }
    #     },
    #     {
    #     'timestamp': '0:11 - 0:20',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2x - 4 = 0',
    #         'position': {
    #             'x': 100,
    #             'y': 100
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '0:21 - 0:30',
    #     'avatar': {
    #         'speak': 'The first step in solving this equation is to isolate x. To do this, we need to get x by itself on one side of the equation. We can do this by adding 4 to both sides of the equation.'
    #     }
    #     },
    #     {
    #     'timestamp': '0:31 - 0:40',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2x - 4 + 4 = 0 + 4',
    #         'position': {
    #             'x': 100,
    #             'y': 150
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '0:41 - 0:50',
    #     'avatar': {
    #         'speak': 'Now that we have x isolated, the next step is to solve for x by dividing both sides of the equation by 2.'
    #     }
    #     },
    #     {
    #     'timestamp': '0:51 - 1:00',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2x / 2 = 4 / 2',
    #         'position': {
    #             'x': 100,
    #             'y': 200
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '1:01 - 1:10',
    #     'avatar': {
    #         'speak': "And there you have it, class! The solution to the equation 2x - 4 = 0 is x = 2. Let's quickly double-check our work by plugging in 2 for x."
    #     }
    #     },
    #     {
    #     'timestamp': '1:11 - 1:20',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2(2) - 4',
    #         'position': {
    #             'x': 100,
    #             'y': 250
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '1:21 - 1:30',
    #     'avatar': {
    #         'speak': 'As you can see, when we plug in 2 for x, we get 0 on the other side of the equation, which means our solution is correct. Great job, class!'
    #     }
    #     },
    #     {
    #     'timestamp': '1:31 - 2:00',
    #     'avatar': {
    #         'speak': "Before we move on to the next topic, does anyone have any questions about solving equations or anything else we've covered so far? I want to make sure everyone is comfortable with these concepts before we move on. Take a moment to think about it, and if you have a question, raise your hand and we'll go over it together."
    #     }
    #     },
    #     {
    #     'timestamp': '2:01 - 2:30',
    #     'avatar': {
    #         'speak': "Alright, class, that's all the time we have for today. Make sure you practice solving equations on your own, and I'll see you all tomorrow. Have a great day!"
    #     }
    #     }
    # ]
    # }[
    # 99
    # ]0stest=json.loads(result)test{
    # 'title': 'Algebra Class: Solving Equations',
    # 'events': [
    #     {
    #     'timestamp': '0:00 - 0:10',
    #     'avatar': {
    #         'speak': "Welcome to algebra class! Today, we're going to learn how to solve equations. Specifically, we're going to solve the equation 2x - 4 = 0 for x. This is an important skill because it allows us to find the value of x when we're given an equation. Let's get started!"
    #     }
    #     },
    #     {
    #     'timestamp': '0:11 - 0:20',
    #     'whiteboard': {
    #         'write table': {
    #         'dimensions': [
    #             200,
    #             50
    #         ],
    #         'content': [
    #             [
    #             '2x - 4 = 0'
    #             ]
    #         ]
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '0:21 - 0:30',
    #     'avatar': {
    #         'speak': 'The first step in solving this equation is to isolate x. To do this, we need to get x by itself on one side of the equation. We can start by adding 4 to both sides of the equation.'
    #     }
    #     },
    #     {
    #     'timestamp': '0:31 - 0:40',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2x - 4 + 4 = 0 + 4',
    #         'position': {
    #             'x': 220,
    #             'y': 30
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '0:41 - 0:50',
    #     'avatar': {
    #         'speak': 'Now that we have x isolated, the next step is to solve for x by dividing both sides of the equation by 2.'
    #     }
    #     },
    #     {
    #     'timestamp': '0:51 - 1:00',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2x / 2 = 4 / 2',
    #         'position': {
    #             'x': 220,
    #             'y': 60
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '1:01 - 1:10',
    #     'avatar': {
    #         'speak': "And there you have it! We have solved the equation 2x - 4 = 0 for x, and the solution is x = 2. Let's double check our work by plugging in 2 for x and seeing if both sides of the equation are equal."
    #     }
    #     },
    #     {
    #     'timestamp': '1:11 - 1:20',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '2(2) - 4',
    #         'position': {
    #             'x': 220,
    #             'y': 90
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '1:21 - 1:30',
    #     'whiteboard': {
    #         'write text': {
    #         'content': '4 - 4 = 0',
    #         'position': {
    #             'x': 350,
    #             'y': 90
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '1:31 - 1:40',
    #     'avatar': {
    #         'speak': 'As you can see, both sides of the equation are equal, so our solution is correct. Great job, class!'
    #     }
    #     },
    #     {
    #     'timestamp': '1:41 - 1:50',
    #     'avatar': {
    #         'speak': "Now, let's practice solving a similar equation. Try solving the equation 3x - 6 = 0 using the same steps we just went over."
    #     }
    #     },
    #     {
    #     'timestamp': '1:51 - 2:00',
    #     'whiteboard': {
    #         'write table': {
    #         'dimensions': [
    #             200,
    #             50
    #         ],
    #         'content': [
    #             [
    #             '3x - 6 = 0'
    #             ]
    #         ]
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '2:01 - 2:10',
    #     'avatar': {
    #         'speak': "Take a few minutes to solve the equation. I'll be here to help if you need it."
    #     }
    #     },
    #     {
    #     'timestamp': '2:11 - 2:20',
    #     'avatar': {
    #         'gesture': 'walking'
    #     }
    #     },
    #     {
    #     'timestamp': '2:21 - 2:30',
    #     'avatar': {
    #         'speak': "Now, let's go over the solution together. What did you get for x?"
    #     }
    #     },
    #     {
    #     'timestamp': '2:31 - 2:40',
    #     'whiteboard': {
    #         'write text': {
    #         'content': 'x = 2',
    #         'position': {
    #             'x': 220,
    #             'y': 120
    #         }
    #         }
    #     }
    #     },
    #     {
    #     'timestamp': '2:41 - 2:50',
    #     'avatar': {
    #         'speak': "In summary, to solve the equation 2x - 4 = 0 for x, we first isolated x by adding 4 to both sides of the equation. Then, we solved for x by dividing both sides of the equation by 2. Finally, we double checked our work by plugging in our solution for x and verifying that both sides of the equation were equal. Remember, this process of isolating and solving for a variable is a fundamental skill in algebra, so keep practicing and you'll get the hang of it!"
    #     }
    #     },
    #     {
    #     'timestamp': '2:51 - 3:00',
    #     'avatar': {
    #         'speak': "Thank you for watching, and I'll see you in the next video!"
    #     }
    #     }
    # ]
    # """

    script = load_json()

    question = question
    script_validator_sys_msg = """You are a script validator. You have to validate the script generated by the agent according to the instructions provided. Provide feedback for the generated script and how to improve it based on the question provided."""

    script_updater_sys_msg = """You are a script updater. Your job is to update the script provided to you according to the feeback that is provided."""

    transformer_sys_msg = """You are an agent who will read transcriptions of a lecture and convert it to an intermediate format where a software application can consume it. The software application has access to following tools:
    1) javascript accessible whiteboard where-in you can write or draw. Whenever you provide any whiteboard action, please provide all the necessary information to perform the action. For ex: If you want to draw any shape you must give the exact coordinates with the markings on the whiteboard. You must remember what is written on the board at any given point in time so that you don’t overwrite.
    2) an avatar who can speak and do gestures./n
    Only output the JSON FILE. nothing else.
    Please convert the following transcription to a valid json format using the interface format given below so that it can be consumed by the software application and can deliver the class:
    """

    interface_template = """
    Interface:
    interface LessonEvent {
        timestamp: string;
        avatar?: {
            speak: string;
            gesture?: string;
        };
        whiteboard?: {
            "write table"?: {
                dimensions: [number, number];
                content: (string | number)[][];
            };
            "write text"?: {
                content: string;
                position: {
                    x: number;
                    y: number;
                };
            };
            "write math equation"?: {
                content: string;
                tex_content: string;
                position: {
                    x: number;
                    y: number;
                };
            };
        };
    }

    interface Lesson {
        title: string;
        events: LessonEvent[];
    }

    """

    transformer_validator_sys_msg = """You are a helpful agent responsible for validating a JSON file and fixing any errors to ensure it strictly adheres to a given interface. When you receive a JSON file, your task is to check it against the provided interface and determine its validity. If you find any errors, you must correct them in the JSON file without modifying the interface.
    To maintain a clean structure and avoid duplication, follow these guidelines:
    1. If a JSON object contains properties that are not specified in the interface, remove those properties from the object.
    2. If a JSON object is missing required properties as defined in the interface, add those properties with appropriate values.
    3. If the structure of the JSON file differs from the interface, restructure the JSON file to match the interface's requirements.
    4. If a JSON object contains an array of elements (e.g., "write math equation") and each element should be a separate object according to the interface, create duplicate objects for each element in the array.
    5. In the duplicate objects created for array elements, include only the necessary properties as defined by the interface and remove any extra properties.
    6. Maintain the order of the objects based on their original position in the JSON file.
    7. JSON requires double quotes for keys and string values, and must be well-formed.
    8. Only output the JSON FILE. Avoid unnecessary pleasantries.
    JSON to validate:\n
    """

    # assign AgentState as an empty dict
    AgentState = {}

    # messages key will be assigned as an empty array. We will append new messages as we pass along nodes.
    AgentState = {"human": [], "ai": [], "script_feedback": []}
    
    
    def generate(state):
        complete_query = sys_msg + "\n\nThe lecture script is given below :\n\n"+script+"\n\nThe question is given below:\n\n" + question
        state["human"].append(complete_query)
        response =  llm.invoke(complete_query)
        if "ai" not in state:
                state["ai"] = []
        state["ai"].append(response.content)
        return state

    def script_validator(state):
        complete_query = script_validator_sys_msg + "\n\nThe prompt is : \n\n" + state["human"][-1] + "\n\nThe script generated is \n\n" + state["ai"][-1]
        response = llm.invoke(complete_query)
        state["script_feedback"].append(response.content)
        return state

    def script_updater(state):
        complete_query = script_updater_sys_msg + "\n\nThe script is given below : \n\n" + state["ai"][-1] + "\n\nHere is additional feedback to be implemented: \n\n" + state["script_feedback"][-1]
        state["human"].append(complete_query)
        response =  llm.invoke(complete_query)
        state["ai"].append(response.content)
        return state


    def human(state):
     return state


    def transform(state):
        complete_query = transformer_sys_msg + interface_template + "\n\nThe script is given below : \n\n"+ state['ai'][-1]
        response =  llm.invoke(complete_query)
        state['ai'].append(response.content)
        return state

    def transformer_validator(state):
        complete_query = transformer_validator_sys_msg + state["ai"][-1] + "\n\nGiven Interface : \n\n" + interface_template
        response = llm.invoke(complete_query)
        state["ai"].append(response.content)
        return state


    def should_continue_script(state):
        print(state["ai"][-1])
        response = input(prompt=f"[y/n] Continue with the given script? ").strip().lower()

    # Decide to continue or end based on the user's input
        if response == "n":
            feedback = input(prompt=f"Enter the changes needed to be made : ")
            state["script_feedback"].append(feedback)
            return "back"
        elif response == "y":
            return "continue"
        else:
            print("Invalid input. Please enter 'y' for yes or 'n' for no.")
            return should_continue_script(state)  # Retry if the input is invalid

    

    workflow = Graph()

    workflow.add_node("script_agent", generate)
    # workflow.add_node("script_validator",script_validator)
    # workflow.add_node("script_updater",script_updater)
    # workflow.add_node("human",human)
    workflow.add_node("transformer", transform)
    workflow.add_node("transformer_validator", transformer_validator)


    # workflow.add_edge("script_agent","script_validator")
    # workflow.add_edge("script_validator","script_updater")
    # # workflow.add_edge("script_updater","human")
    # workflow.add_edge("script_updater","transformer")
    # workflow.add_edge("transformer","transformer_validator")

    # workflow.set_entry_point("script_agent")
    # workflow.set_finish_point("transformer_validator")
    workflow.add_edge("script_agent","transformer")
    workflow.add_edge("transformer","transformer_validator")
    workflow.set_entry_point("script_agent")
    workflow.set_finish_point("transformer_validator")


    # workflow.add_conditional_edges(
    #     "human",
    #     should_continue_script,
    #     {
    #         "back": "script_updater",
    #         "continue": "transformer",
    #     },
    # )



    app = workflow.compile()
    app.invoke(AgentState)
    result  = AgentState["ai"][-1]
    
    start = result.find("{")
    end = result.rfind("}") + 1
    json_string = result[start:end]
   


    data = json.loads(json_string)
    jsonobj = json.dumps(data)
    
    return jsonobj
