let currentY = 0;
let margin = 100;
let title = undefined;
let enableSpeech = true;
let isPaused = false;
let currentIndex = 0;
let currentIndexAnswer = 0;
let playIcon = "▶";
let pauseIcon = "❚❚";
let isQuestionMode = false;
let answer = undefined;
let textData = undefined;

const loadJSON = async (url) => {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Failed to load ${url}: ${response.statusText}`);
  }
  return await response.json();
};

function pauseButtonCallback() {
  isPaused = !isPaused;
  if (!isPaused) {
    // play
    document.getElementById("pause-button").textContent = pauseIcon;
    // disable speak-button
    document.getElementById("speak-button").disabled = true;
    responsiveVoice.resume();
    if (isQuestionMode) {
      processItems(answer.events, currentIndexAnswer).then(() => {
        isQuestionMode = false;
        processItems(textData.events, currentIndex);
      });
    } else {
      processItems(textData.events, currentIndex);
    }
  } else {
    responsiveVoice.pause();
    document.getElementById("pause-button").textContent = playIcon;
    document.getElementById("speak-button").disabled = false;
    if (isQuestionMode) {
      displayPopup();
    }
  }
}

/**
 * Display popup with 3 options
 * 1. Continue
 * 2. Resume lecture
 * 3. Ask another question
 */
function displayPopup() {
  // Display a prompt with three options
  let userChoice = window.prompt(
    "Please enter your choice: 1. Continue, 2. Resume lecture, 3. Ask another question"
  );

  // Based on the user's choice, perform an action
  switch (userChoice) {
    case "1":
      // Continue
      pauseButtonCallback();
      break;
    case "2":
      // Resume lecture
      isQuestionMode = false;
      answer = undefined;
      currentIndexAnswer = 0;
      pauseButtonCallback();
      break;
    case "3":
      // Ask another question
      isQuestionMode = false;
      answer = undefined;
      currentIndexAnswer = 0;
      break;
    default:
      console.log("Invalid choice");
  }
}

export async function setupApp(textDataArg) {
  console.log(textDataArg);
  console.log(typeof textDataArg);
  textData = textDataArg;
  setRootDocumentCss();

  handleTitle(textData);

  document.getElementById("pause-button").textContent = pauseIcon;
  responsiveVoice.cancel();
  // Create a separate container for the content
  // let contentContainer = document.createElement('div');
  // contentContainer.id = 'content_container';
  // document.getElementById('root_document').appendChild(contentContainer);

  document
    .getElementById("pause-button")
    .addEventListener("click", pauseButtonCallback);

  document
    .getElementById("speak-button")
    .addEventListener("click", async function () {
      let question = document.getElementById("question-input").value;
      const spinner = document.querySelector(".spinner");

      // Show spinner while waiting for response
      spinner.style.display = "block";

      fetch("/ask", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ question: question }),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Response from Flask:", data);
          console.log(typeof data);
          answer = data;
          let questionCell = document.createElement("div");

          questionCell.className = "question-cell";
          let questionCellTitle = document.createElement("h2");
          questionCellTitle.textContent = "Questions";
          questionCellTitle.style.fontSize = "24px";
          questionCellTitle.style.textAlign = "center";
          questionCell.appendChild(questionCellTitle);
          // change background color of question-cell slightly
          document.getElementById("root_document").appendChild(questionCell);

          // add the question to the question cell
          let questionText = document.createElement("h3");
          questionText.textContent = question;
          questionCell.appendChild(questionText);
          // align to center
          questionText.style.textAlign = "center";
          isQuestionMode = true;
          // update color
          questionText.style.color = "red";
        })
        .catch((error) => console.error("Error:", error))
        .finally(() => {
          // Hide spinner when response is received (success or error)
          spinner.style.display = "none";
          pauseButtonCallback();
        });
      // console.log(await loadJSON("/static/data/lecturePayloadAddition.json"));

      // call backend api and get the response
      // we are templorarily going to use this hardcoded response
      // answer = await loadJSON("/static/data/lecturePayloadAddition.json");
    });

  document.getElementById("speak-button").disabled = true;

  processItems(textData.events, currentIndex).then(() => {
    console.log("All items have been processed");
  });
}

function handleTitle(textData) {
  let titleText = textData.title;

  // Create a title element with specific styling
  title = document.createElement("h1");
  title.textContent = titleText;
  // title.style.fontFamily = "Comic Sans MS";
  // title.style.fontSize = "36px";
  // title.style.fontWeight = "bold";
  // title.style.color = "#000000";
  // title.style.textAlign = "center";
  // title.style.position = "sticky"; // Make the title sticky
  // title.style.top = "0"; // Position the title at the top of the page
  // title.style.width = "100%"; // Set the width to 100%
  // title.style.opacity = "1"; // Set the opacity to 0.9
  // title.style.alpha = 1; // Set the opacity to 1
  // // title.style.backgroundColor = '#FFFFFF'; // Set the background color to white
  // title.style.zIndex = "9999"; // Set the z-index to a high value
  // // Add the title to the root_document
  document.getElementById("title-div").appendChild(title);

  // Initialize the current Y position
  currentY = title.offsetHeight + margin;
}
function setRootDocumentCss() {
  document.getElementById("root_document").style.display = "flex";
  document.getElementById("root_document").style.flexDirection = "row";
  document.getElementById("root_document").style.alignItems = "flex-start";
  document.getElementById("root_document").style.justifyContent =
    "space-around";
}

async function processItems(items, startIndex) {
  for (let i = startIndex; i < items.length; i++) {
    if (isQuestionMode) {
      currentIndexAnswer = i;
    } else {
      currentIndex = i;
    }
    if (isPaused) {
      break;
    }
    await processItem(items[i]);
  }
}

async function processItem(item) {
  // handle 'whiteboard' actions
  if (item.whiteboard) {
    // handle 'write table' action
    if (item.whiteboard["write table"]) {
      let table = document.createElement("table");
      applyTextStyle(table);
      for (let row of item.whiteboard["write table"].content) {
        let tr = document.createElement("tr");
        for (let cell of row) {
          let td = document.createElement("td");
          td.textContent = cell;
          tr.appendChild(td);
        }
        table.appendChild(tr);
      }
      applyMathTableStyle(table);
      addCellToDocument(table);
    }

    // handle 'write text' action
    if (item.whiteboard["write text"]) {
      let text = document.createElement("p");
      text.textContent = item.whiteboard["write text"].content;
      applyTextStyle(text);
      addCellToDocument(text);
    }

    // handle 'write math equation' action
    if (item.whiteboard["write math equation"]) {
      let tex = document.createElement("p");
      katex.render(item.whiteboard["write math equation"].tex_content, tex, {
        throwOnError: false,
      });
      applyTextStyle(tex);
      addCellToDocument(tex);
    }

    // handle 'draw angle' action
    if (item.whiteboard["draw angle"]) {
      let angleData = item.whiteboard["draw angle"];
      let pixiApp = drawAngle(
        angleData.vertex,
        angleData.arm1Length,
        angleData.arm2Length,
        angleData.angle,
        angleData.direction
      );
      addCellToDocument(pixiApp.view);
    }

    // Scroll to the bottom of the content_container
    let contentContainer = document.getElementById("content_container");

    if (contentContainer && contentContainer.lastElementChild) {
      contentContainer.lastElementChild.scrollIntoView({ behavior: "smooth" });
    }
  }

  if (!enableSpeech) {
    return new Promise((resolve) => {
      setTimeout(resolve, 2000);
    });
  }

  // Speak the text using responseVoices and return a promise that resolves 2 seconds after the endCallback is called
  return new Promise((resolve) => {
    // handle 'avatar' action
    if (item.avatar && item.avatar.speak) {
      responsiveVoice.speak(item.avatar.speak, "UK English Female", {
        onend: function () {
          setTimeout(resolve, 2000);
        },
      });
      return;
    }

    return resolve();
  });
}

function drawAngle(vertex, arm1Length, arm2Length, angle, direction) {
  // app should be a window 2 times the size of largest ar
  let app = new PIXI.Application({ width: 300, height: 300 });

  // update vertex to be center of app
  vertex.x = app.screen.width / 2;
  vertex.y = app.screen.height / 2;

  // change background color to light yellow
  app.renderer.backgroundColor = 0xffffff;

  // Create a new Graphics object
  let graphics = new PIXI.Graphics();

  let size = 50;
  // 10 equally spaced horizontal lines
  for (let i = 0; i < size; i++) {
    graphics.lineStyle(1, 0x000000, 0.1);
    graphics.moveTo(0, (i * app.screen.height) / size);
    graphics.lineTo(app.screen.width, (i * app.screen.height) / size);
  }

  // 10 equally spaced vertical lines
  for (let i = 0; i < size; i++) {
    graphics.lineStyle(1, 0x000000, 0.1);
    graphics.moveTo((i * app.screen.width) / size, 0);
    graphics.lineTo((i * app.screen.width) / size, app.screen.height);
  }

  // Draw the first arm
  graphics.lineStyle(2, 0x0000ff, 1);
  graphics.moveTo(vertex.x, vertex.y);
  graphics.lineTo(vertex.x + arm1Length, vertex.y);

  // Convert the angle to radians
  let angleInRadians = (angle * Math.PI) / 180;

  // If the direction is clockwise, subtract the angle from 180 degrees
  if (direction === "clockwise") {
    angleInRadians = Math.PI - angleInRadians;
  }

  // Calculate the end point of the second arm
  let endX, endY;
  if (angle <= 90) {
    // If the angle is acute, the end point is to the right of the first arm
    endX = vertex.x + arm2Length * Math.cos(angleInRadians);
    endY = vertex.y - arm2Length * Math.sin(angleInRadians);
  } else {
    // If the angle is obtuse, the end point is to the left of the first arm
    endX = vertex.x - arm2Length * Math.cos(Math.PI - angleInRadians);
    endY = vertex.y - arm2Length * Math.sin(Math.PI - angleInRadians);
  }

  // Draw the second arm
  graphics.moveTo(vertex.x, vertex.y);
  graphics.lineTo(endX, endY);

  // Create new text objects
  let textA = new PIXI.Text("A", {
    fontFamily: "Arial",
    fontSize: 24,
    fill: 0xff1010,
    align: "center",
  });
  let textB = new PIXI.Text("B", {
    fontFamily: "Arial",
    fontSize: 24,
    fill: 0xff1010,
    align: "center",
  });
  let textC = new PIXI.Text("C", {
    fontFamily: "Arial",
    fontSize: 24,
    fill: 0xff1010,
    align: "center",
  });

  // Call the function for each text object
  positionText(textA, vertex, vertex);
  positionText(textB, { x: vertex.x + arm1Length, y: vertex.y }, vertex);
  positionText(textC, { x: endX, y: endY }, vertex);

  app.stage.addChild(graphics);

  // draw cicles at the end of the arms and vertex
  graphics.beginFill(0xff0000);
  graphics.drawCircle(vertex.x, vertex.y, 2);
  graphics.drawCircle(vertex.x + arm1Length, vertex.y, 2);
  graphics.drawCircle(endX, endY, 2);
  graphics.endFill();

  // draw an arc to show the angle
  graphics.lineStyle(2, 0x000000, 1);
  graphics.arc(
    vertex.x,
    vertex.y,
    20,
    0,
    2 * Math.PI - angleInRadians,
    direction === "anticlockwise"
  );

  // Draw angle label
  let angleLabel = new PIXI.Text(`${angle}°`, {
    fontFamily: "Arial",
    fontSize: 20,
    fill: 0x000000,
    align: "center",
  });
  angleLabel.x = vertex.x + 30;
  angleLabel.y = vertex.y - 30;
  app.stage.addChild(angleLabel);

  // Add the text objects to the stage
  app.stage.addChild(textA);
  app.stage.addChild(textB);
  app.stage.addChild(textC);

  return app;
}

function applyTextStyle(element) {
  element.style.fontFamily = "Comic Sans MS";
  element.style.fontSize = "24px";
  element.style.color = "#000000";
  element.style.textAlign = "center";
  // element.style.marginBottom = "100px"; // Add a margin to the bottom of the element
}

function applyMathTableStyle(table) {
  // Prevent border collapse
  table.style.borderCollapse = "collapse";
  // align the table to the center
  table.style.margin = "0 auto";

  // Get the last row of the table
  let lastRow = table.rows[table.rows.length - 1];

  // Apply styles to the last row
  lastRow.style.borderTop = "2px solid #000";
  lastRow.style.borderBottom = "2px solid #000";

  // Set minimum height for each row
  for (let i = 0; i < table.rows.length; i++) {
    table.rows[i].style.height = "35px"; // Change '50px' to the minimum height you want
  }
}

function addCellToDocument(cell) {
  let cellDiv = document.createElement("div");
  cellDiv.appendChild(cell);
  // cellDiv.style.backgroundColor = 'aliceblue';
  // cellDiv.style.margin = "0 0 100px 0";
  setTimeout(() => {
    // cellDiv.style.backgroundColor = '#FFFFFF'
  }, 1000);
  if (isQuestionMode) {
    let container = document.getElementById("content_container");
    console.log(container);
    // position: absolute;
    // left: 100px;
    // top: 100px;

    container.style.position = "relative";
    // container.style.top = "100px";
    // container.style.left = "100px";
    // use the last question cell
    let questionCells = document.getElementsByClassName("question-cell");
    questionCells[questionCells.length - 1].appendChild(cellDiv);
  } else {
    document.getElementById("content_container").appendChild(cellDiv);
  }
}

function positionText(text, point, vertex) {
  let buffer = 20; // Adjust this value as needed

  // Calculate the position of the text
  let x = point.x - vertex.x;
  let y = point.y - vertex.y;

  // lies on origin
  if (x === 0 && y === 0) {
    text.x = vertex.x;
    text.y = vertex.y + buffer;
    return;
  }

  // lies on x-axis
  if (y === 0) {
    text.x = point.x;
    text.y = point.y + buffer;
    return;
  }

  // lies on y-axis
  if (x === 0) {
    text.x = point.x - buffer;
    text.y = point.y - buffer;
    return;
  }

  // 1st or 4th quadrant to the right
  if (x > 0) {
    text.x = point.x + buffer * 1;
    // 1st quadrant up
    if (y < 0) {
      text.y = point.y - buffer * 1;
    } else {
      // 4th quadrant down
      text.y = point.y + buffer * 1;
    }
  } else {
    // 2nd or 3rd quadrant to the left
    text.x = point.x - buffer * 1;
    // 2nd quadrant up
    if (y < 0) {
      text.y = point.y - buffer * 1;
    } else {
      // 3rd quadrant down
      text.y = point.y + buffer * 1;
    }
    text.y = point.y - buffer * 1;
  }
}
