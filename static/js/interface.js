interface LessonEvent {
    timestamp: string;
    avatar?: {
        speak: string;
        gesture?: string;
    };
    whiteboard?: {
        "write table"?: {
            dimensions: [number, number];
            content: (string | number)[][];
        };
        "write text"?: {
            content: string;
            position: {
                x: number;
                y: number;
            };
        };
        "write math equation"?: {
            content: string;
            tex_content: string;
            position: {
                x: number;
                y: number;
            };
        };
    };
}

interface Lesson {
    title: string;
    events: LessonEvent[];
}