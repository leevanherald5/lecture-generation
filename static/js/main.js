import { setupApp } from "./pixiApp.js";
// import lecturePayload from '../data/lecturePayload.json' assert { type: "json" };
// import lecturePayload from '../data/lecturePayloadWithLatex.json' assert { type: "json" };
// import lecturePayloadQuadratic from "../data/lecturePayloadWithLatex2.json" assert { type: "json" };
// import lecturePayloadAddition from "../data/lecturePayloadAddition.json" assert { type: "json" };
// import lecturePayloadAngles from "../data/lecturePayloadAngles.json" assert { type: "json" };

//toggle sidebar
var btn = document.querySelector(".toggle");
var btnst = true;
btn.onclick = function () {
  if (btnst == true) {
    document.querySelector(".toggle span").classList.add("toggle");
    document.getElementById("nav").classList.add("navshow");
    btnst = false;
  } else if (btnst == false) {
    document.querySelector(".toggle span").classList.remove("toggle");
    document.getElementById("nav").classList.remove("navshow");
    btnst = true;
  }
};

const loadJSON = async (url) => {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Failed to load ${url}: ${response.statusText}`);
  }
  return await response.json();
};

window.onload = async () => {
  // based on the query param I want to switch between lecturePayloadQuadratic or LecturePayloadAddition
  const urlParams = new URLSearchParams(window.location.search);
  const lectureType = urlParams.get("lectureType");
  let lecturePayloadUrl;

  switch (lectureType) {
    case "addition":
      lecturePayloadUrl = "/static/data/lecturePayloadAddition.json";
      break;
    case "angles":
      lecturePayloadUrl = "/static/data/lecturePayloadAngles.json";
      break;
    case "quadratic":
    default:
      lecturePayloadUrl = "/static/data/lecturePayloadWithLatex2.json";
      break;
  }

  try {
    const lecturePayload = await loadJSON(lecturePayloadUrl);
    setupApp(lecturePayload);
  } catch (error) {
    console.error("Error loading lecture payload:", error);
  }
};
