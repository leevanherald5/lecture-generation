FROM python:3.9-slim

WORKDIR /app

COPY . /app

RUN python3 -m venv venv

RUN . venv/bin/activate

RUN pip install -r requirements.txt

ENV FLASK_APP=app.py

EXPOSE 5000

CMD ["flask","run","--host", "0.0.0.0"]
